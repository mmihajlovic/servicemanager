﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using System.ComponentModel;
using System.ServiceProcess;
using System.Diagnostics;
using System.Windows.Forms.VisualStyles;
using System.IO;
using Microsoft.Win32;

namespace ServiceManager
{
    class MainProg : ApplicationContext
    {
        [DllImport("user32")]
        private static extern bool DestroyIcon(IntPtr hIcon); //http://www.dotnetmonster.com/Uwe/Forum.aspx/dotnet-drawing/2211/Bug-Suspicion-So-any-gdi-object-Dispose-does-what-exactly

        string _settingsFilePath;
        IContainer _container;
        NotifyIcon _icon;
        bool _disposed;
        List<ServiceController> _serviceControllers;
        Timer _timer;
        RegistryKey regSystemUsesLightThemeKey;
        int? regSystemUsesLightTheme = null;

        public MainProg()
        {
            _container = new Container();

            _settingsFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ServiceManager", "settings.xml");

            //TODO: List od services
            _serviceControllers = new List<ServiceController>();

            _icon = new NotifyIcon(_container);

            //Computer\HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize\SystemUsesLightTheme
            regSystemUsesLightThemeKey = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Default)
                .OpenSubKey("Software")
                .OpenSubKey("Microsoft")
                .OpenSubKey("Windows")
                .OpenSubKey("CurrentVersion")
                .OpenSubKey("Themes")
                .OpenSubKey("Personalize");

            regSystemUsesLightTheme = regSystemUsesLightThemeKey.GetValue("SystemUsesLightTheme") as int?;

            if (regSystemUsesLightTheme == 1)
            {
                _icon.Icon = new Icon(Properties.Resources.cogs_icon_black, new Size(16, 16));
            }
            else
            {
                _icon.Icon = new Icon(Properties.Resources.cogs_icon_white, new Size(16, 16));
            }

            _icon.Text = "Service Manager";

            //var _allServices = ServiceController.GetServices();
            var _serviceConfig = GetConfig();

            CreateMenuItems(_serviceConfig);

            _timer = new Timer(_container);
            _timer.Interval = 2500;
            _timer.Tick += (s, e) =>
            {
                // refresh statuses
                _icon.ContextMenuStrip.Items.OfType<ServiceMenuItem>().ToList().ForEach(item => item.Refresh());

                // refresh icon
                var newVal = regSystemUsesLightThemeKey.GetValue("SystemUsesLightTheme") as int?;

                if (newVal != regSystemUsesLightTheme)
                {
                    regSystemUsesLightTheme = newVal;
                    if (regSystemUsesLightTheme == 1)
                    {
                        _icon.Icon = new Icon(Properties.Resources.cogs_icon_black, new Size(16, 16));
                    }
                    else
                    {
                        _icon.Icon = new Icon(Properties.Resources.cogs_icon_white, new Size(16, 16));
                    }
                }

            };
            _timer.Enabled = true;

            _icon.MouseDoubleClick += StartServices;

            _icon.Visible = true;
        }

        private void CreateMenuItems(ServiceManagerSettings settings)
        {
            _icon.ContextMenuStrip = new ServiceContextMenu();

            // Add services button
            var def = _icon.ContextMenuStrip.Items.Add("Services", null, StartServices); //.DefaultItem = true;
            def.Font = new Font(def.Font, FontStyle.Bold);
            _icon.ContextMenuStrip.Items.Add("Event Log", null, StartEventLog);
            _icon.ContextMenuStrip.Items.Add("Settings", null, ShowSettings);
            _icon.ContextMenuStrip.Items.Add("-");

            // Add the links to the actual services from the .config file
            _icon.ContextMenuStrip.Items.AddRange(settings?.Services.Select(s => new ServiceMenuItem(s)).ToArray());
            _icon.ContextMenuStrip.Items.OfType<ServiceMenuItem>().ToList().ForEach(item => item.StatusChanged += item_StatusChanged);

            // Add exit button
            _icon.ContextMenuStrip.Items.Add("-");
            _icon.ContextMenuStrip.Items.Add("Exit", null, ExitServiceManager);
        }

        private ServiceManagerSettings GetConfig()
        {
            return ServiceManagerSettings.load(_settingsFilePath);
        }

        void item_StatusChanged(object sender, EventArgs e)
        {
            var item = sender as ServiceMenuItem;

            _icon.ShowBalloonTip(10000, "Status Change", (item.ServiceElement.DisplayName ?? item.Service.DisplayName) + " status changed to " + Humanize(item.Service.Status), ToolTipIcon.Info);
        }

        ~MainProg()
        {
            Dispose(false);
        }

        private string Humanize(ServiceControllerStatus status)
        {
            switch (status)
            {
                case ServiceControllerStatus.ContinuePending:
                    return "continue pending";
                case ServiceControllerStatus.Paused:
                    return "paused";
                case ServiceControllerStatus.PausePending:
                    return "pause pending";
                case ServiceControllerStatus.Running:
                    return "running";
                case ServiceControllerStatus.StartPending:
                    return "start pending";
                case ServiceControllerStatus.Stopped:
                    return "stopped";
                case ServiceControllerStatus.StopPending:
                    return "stop pending";
                default:
                    return "unknown";
            }
        }

        private void ShowSettings(object sender, EventArgs e)
        {
            if ((e is MouseEventArgs == false) || ((MouseEventArgs)e).Button == MouseButtons.Left)
            {

                var f = new frmSettings();
                var settings = GetConfig();

                if (f.ShowDialog(ref settings) == DialogResult.OK)
                {
                    ServiceManagerSettings.Save(_settingsFilePath, settings);

                    CreateMenuItems(settings);
                }
            }
        }

        private void StartServices(object sender, EventArgs e)
        {
            if ((e is MouseEventArgs == false) || ((MouseEventArgs)e).Button == MouseButtons.Left)
            {
                Process.Start("services.msc");
            }
        }

        private void StartEventLog(object sender, EventArgs e)
        {
            if ((e is MouseEventArgs == false) || ((MouseEventArgs)e).Button == MouseButtons.Left)
            {
                Process.Start("eventvwr.msc");
            }
        }

        private void ExitServiceManager(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit Service Manager?", "Exit Service Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                //DestroyIcon(_icon_cog.Handle);

                _timer.Enabled = false;

                _icon.ContextMenuStrip.Items.OfType<ServiceMenuItem>().ToList().ForEach(item => item.StatusChanged -= item_StatusChanged);

                _disposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
