﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using System.Security.Permissions;
using System.Threading;
using System.Security.Principal;

namespace ServiceManager
{
    class ServiceMenuItem : ToolStripMenuItem
    {
        public event EventHandler StatusChanged;

        public ServiceController Service { get; private set; }
        public ServiceSetting ServiceElement { get; private set; }
        public ServiceControllerStatus PreviousStatus { get; set; }

        public ServiceMenuItem(ServiceSetting serviceElement) : base()
        {
            this.ServiceElement = serviceElement;

            var _service = ServiceController.GetServices().Where(s => s.ServiceName == ServiceElement.ServiceName).FirstOrDefault();
            
            if (_service != null)
            {
                this.Service = _service;
                this.Enabled = true;
                this.PreviousStatus = Service.Status;

                this.Image = SystemIcons.Shield.ToBitmap();
                this.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
                this.ImageAlign = ContentAlignment.MiddleLeft;
                this.TextAlign = ContentAlignment.MiddleRight;

                //this.Checked = false;
            }
            else
            {
                this.Enabled = false;
            }
        }

        public void Refresh()
        {
            if (this.Enabled && this.Service != null)
            {
                try
                {
                    this.Service.Refresh();

                    if (!Thread.CurrentPrincipal.IsInRole(@"BUILTIN\Administrators"))
                    {
                        this.ImageIndex = 0;
                    }
                    //■ ►
                    switch (this.Service.Status)
                    {
                        case ServiceControllerStatus.Running:
                        case ServiceControllerStatus.StartPending:
                            //this.ImageIndex = 2;
                            this.Checked = true;
                            break;

                        case ServiceControllerStatus.Stopped:
                        case ServiceControllerStatus.StopPending:
                            //this.ImageIndex = 1;
                            this.Checked = false;
                            break;

                        case ServiceControllerStatus.Paused:
                        case ServiceControllerStatus.PausePending:
                        default:
                            //this.ImageIndex = 3;
                            this.Checked = false;
                            break;

                    }

                    this.Text = string.Format("{0} \t({1})", this.ServiceElement.DisplayName ?? this.Service.DisplayName, this.Service.Status);

                    if (this.Service.Status != PreviousStatus)
                    {
                        PreviousStatus = this.Service.Status;
                        StatusChanged(this, EventArgs.Empty);
                    }
                }
                catch (Exception ex)
                {
                    this.Checked = false;
                    this.Enabled = false;
                    this.Text = string.Format("{0} (No service found)", this.ServiceElement.DisplayName ?? this.ServiceElement.ServiceName);
                }
            }
            else
            {
                this.Checked = false;
                this.Text = string.Format("{0} (No service found)", this.ServiceElement.DisplayName ?? this.ServiceElement.ServiceName);
            }
        }


        //[PrincipalPermission(SecurityAction.Demand, Role = @"BUILTIN\Administrators")]
        protected override void OnClick(EventArgs e)
        {
            Service.Refresh();

            switch (Service.Status)
            {
                case ServiceControllerStatus.Stopped:
                case ServiceControllerStatus.Paused:
                    {
                        try
                        {
                            if (!Program.IsAdministrator())
                            {
                                // Restart program and run as admin
                                Program.StartAdminProcess("net", "start " + Service.ServiceName);
                                return;
                            }
                            else
                            {
                                Service.Start();
                            }
                        }
                        catch { }
                        break;
                    }
                case ServiceControllerStatus.Running:
                    {
                        try
                        {
                            if (!Program.IsAdministrator())
                            {

                                Program.StartAdminProcess("net", "stop " + Service.ServiceName);
                                return;
                            }
                            else
                            {
                                Service.Stop();
                            }
                        }
                        catch { }
                        break;
                    }
                default:
                    {
                        Debug.Print(Service.Status.ToString());
                        //Do nothing
                        break;
                    }
            }
        }
    }
}
