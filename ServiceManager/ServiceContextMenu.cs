﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;
using System.ComponentModel;
using System.Drawing;
using System.Threading;

namespace ServiceManager
{
    class ServiceContextMenu : ContextMenuStrip
    {
        public ServiceContextMenu()
        {
            this.RenderMode = ToolStripRenderMode.Professional;
            if (Program.IsAdministrator() == false)
            {
                this.ShowImageMargin = this.ShowCheckMargin = true;
            }
            else
            {
                this.ShowImageMargin = false;
                this.ShowCheckMargin = true;
            }

            ImageList il = new ImageList();

            il.Images.Add(ResizeImage(SystemIcons.Shield.ToBitmap(), 16, 16));
            il.Images.Add(Properties.Resources.stop);
            il.Images.Add(Properties.Resources.running);
            il.Images.Add(Properties.Resources.paused);

            this.ImageList = il;
            
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            //a holder for the result
            Bitmap result = new Bitmap(width, height);
            //set the resolutions the same to avoid cropping due to resolution differences
            result.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            //use a graphics object to draw the resized image into the bitmap
            using (Graphics graphics = Graphics.FromImage(result))
            {
                //set the resize quality modes to high quality
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                //draw the image into the target bitmap
                graphics.DrawImage(image, 0, 0, result.Width, result.Height);
            }

            //return the resulting bitmap
            return result;
        }


        protected override void OnOpening(CancelEventArgs e)
        {
            foreach (var item in this.Items.OfType<ServiceMenuItem>())
            {
                item.Refresh();
            }

            base.OnOpening(e);
        }
    }
}
