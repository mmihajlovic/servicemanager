; example1.nsi
;
; This script is perhaps one of the simplest NSIs you can make. All of the
; optional settings are left to their default settings. The installer simply 
; prompts the user asking them where to install, and drops a copy of example1.nsi
; there. 

;--------------------------------

; The name of the installer
Name "Service Manager"

; The file to write
OutFile "bin\Release\service_manager.exe"

; The default installation directory
InstallDir "$PROGRAMFILES\ServiceManager"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

;--------------------------------

; Pages
Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles
;--------------------------------

; The stuff to install
Section "Directory" ;No components page, name is not important
  SectionIn RO
  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"
  
  ; Put file there
  File bin\Release\ServiceManager.exe
  ;File bin\Release\ServiceManager.exe.config

  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Run" "ServiceManager" "$PROGRAMFILES\ServiceManager\ServiceManager.exe"
  
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\ServiceManager" "DisplayName" "Service Manager"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\ServiceManager" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteUninstaller $INSTDIR\uninstall.exe

SectionEnd ; end the section

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Service Manager"
  CreateShortcut "$SMPROGRAMS\Service Manager\Uninstall.lnk" "$INSTDIR\uninstall.exe"
  CreateShortcut "$SMPROGRAMS\Service Manager\Service Manager.lnk" "$INSTDIR\ServiceManager.exe"
  
SectionEnd

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegValue HKCU "Software\Microsoft\Windows\CurrentVersion\Run" "ServiceManager"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\ServiceManager"

  ; Remove files and uninstaller
  Delete $INSTDIR\ServiceManager.exe
  ;Delete $INSTDIR\ServiceManager.exe.config
  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\Service Manager\*.*"

  ; Remove directories used
  RMDir "$INSTDIR"

SectionEnd