﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace ServiceManager
{
    [Serializable]
    public sealed class ServiceManagerSettings
    {
        public List<ServiceSetting> Services { get; set; }

        public static ServiceManagerSettings load(string file)
        {
            var ser = new XmlSerializer(typeof(ServiceManagerSettings));

            if (File.Exists(file) == false)
            {
                return new ServiceManagerSettings()
                {
                    Services = new List<ServiceSetting>()
                };
            }

            using (var f = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                return ser.Deserialize(f) as ServiceManagerSettings;
                //f.Close();
            }
        }

        public static void Save(string file, ServiceManagerSettings settings)
        {
            var ser = new XmlSerializer(typeof(ServiceManagerSettings));

            Directory.CreateDirectory(Path.GetDirectoryName(file));

            using (var f = new FileStream(file, FileMode.Create, FileAccess.Write))
            {
                ser.Serialize(f, settings);
                f.Close();
            }
        }
    }

    [Serializable]
    public class ServiceSetting
    {
        public string ServiceName { get; set; }
        public string DisplayName { get; set; }
    }
}