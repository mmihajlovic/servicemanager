﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Windows.Forms;

namespace ServiceManager
{
    public partial class frmSettings : Form
    {
        List<ServiceItem> serviceItems;
        RegistryKey startupRoot = Registry.CurrentUser
            .CreateSubKey("SOFTWARE")
            .CreateSubKey("Microsoft")
            .CreateSubKey("Windows")
            .CreateSubKey("CurrentVersion")
            .CreateSubKey("Run");

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

        public frmSettings()
        {
            InitializeComponent();

            var btn = new PictureBox();
            btn.Size = new Size(16, uiSearchBox.ClientSize.Height + 2);
            
            btn.Dock = DockStyle.Right;
            btn.Cursor = Cursors.Default;
            btn.Image = Properties.Resources.cross;
           
            //btn.FlatStyle = FlatStyle.Flat;
            btn.ForeColor = Color.White;
            //btn.FlatAppearance.BorderSize = 0;
            btn.Click += (sender, e) => { uiSearchBox.Text = ""; };
            uiSearchBox.Controls.Add(btn);

            // Send EM_SETMARGINS to prevent text from disappearing underneath the button
            SendMessage(uiSearchBox.Handle, 0xd3, (IntPtr)2, (IntPtr)(btn.Width << 16));
        }

        public DialogResult ShowDialog(ref ServiceManagerSettings settings)
        {
            PopulateSettings(settings);

            var result = this.ShowDialog();

            if (result == DialogResult.OK)
            {
                settings.Services = (from s in serviceItems
                                    where s.Selected
                                    select new ServiceSetting()
                                    {
                                        ServiceName = s.ServiceName,
                                        DisplayName = s.DisplayName
                                    }).ToList();

                if (uiRunOnStartup.Checked)
                {
                    startupRoot.SetValue("ServiceManager", System.Reflection.Assembly.GetExecutingAssembly().Location);

                }
                else
                {
                    startupRoot.DeleteValue("ServiceManager", false);
                }
            }

            return result;
        }

        private void PopulateSettings(ServiceManagerSettings settings)
        {
            var _services = from service in ServiceController.GetServices()
                            let serviceSetting = (settings != null && settings.Services != null) ? settings.Services.FirstOrDefault(s => s.ServiceName == service.ServiceName) : null
                            orderby service.ServiceName
                            select new ServiceItem()
                            {
                                Service = service,
                                DisplayName = serviceSetting?.DisplayName ?? service.DisplayName,
                                Selected =  (serviceSetting != null)
                            };

            serviceItems = _services.ToList();
            serviceItemBindingSource.DataSource = serviceItems;

            //split the two name columns in two
            serviceNameDataGridViewTextBoxColumn.Width = (serviceNameDataGridViewTextBoxColumn.Width + displayNameDataGridViewTextBoxColumn.Width) / 2;

            //run on startup
            uiRunOnStartup.Checked = (startupRoot.GetValue("ServiceManager") != null);

        }

        public class ServiceItem
        {
            public ServiceController Service { get; set; }
            public string ServiceName
            {
                get
                {
                    return Service.ServiceName;
                }
            }

            public string DisplayName { get; set; }

            public bool Selected { get; set; }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            serviceItemBindingSource.DataSource = string.IsNullOrWhiteSpace(uiSearchBox.Text) 
                ? serviceItems 
                : serviceItems.Where(item => item.ServiceName.IndexOf(uiSearchBox.Text, StringComparison.InvariantCultureIgnoreCase) > -1 ||
                    item.DisplayName.IndexOf(uiSearchBox.Text, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
        }

        private void uiOk_Click(object sender, EventArgs e)
        {
        }
    }
}
